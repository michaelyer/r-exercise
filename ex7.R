contact.raw<-read.csv("contact.csv",stringsAsFactors = FALSE)
contact<-contact.raw
myStopwords = readLines("stopwords.csv")
str(contact)
summary(contact)

# Transforming the category column to a factor
contact$category <- as.factor(contact$category)
library(ggplot2)
ggplot(contact,aes(category))+geom_bar()
# accoring to the plot we can see that there are more contacts in the sales department

# a package to deal with text strings
install.packages('tm')
library(tm)


contact_corpus<-Corpus(VectorSource(contact$text))
contact_corpus[[3]][[1]]

# removing punctuation with corpus function
clean_corpus<-tm_map(contact_corpus,removePunctuation)
clean_corpus[[3]][[1]]

# removing numeric values
clean_corpus<-tm_map(clean_corpus,removeNumbers)
clean_corpus[[2]][[1]]

# removing spaces
clean_corpus<-tm_map(clean_corpus,stripWhitespace)
clean_corpus[[1]][[1]]

# removing stopwords
clean_corpus <- tm_map(clean_corpus, removeWords, myStopwords)
clean_corpus[[3]][[1]]

# removoe unnecessary spaces
clean_corpus <- tm_map(clean_corpus,stripWhitespace)
clean_corpus[[2]][[1]]

# we want to work on a smaller compact version of the dataframe
dtm<-DocumentTermMatrix(clean_corpus)
dim(dtm) 

# We remove words that appear less then 29 times, that way we can filter unwanted garbage from our data
frequent_dtm<-DocumentTermMatrix(clean_corpus, list(dictionary=findFreqTerms(dtm,29)))
dim(frequent_dtm)


#intalling and loading librariese that help us create word clouds
install.packages('wordcloud')
library(wordcloud)
install.packages("SnowballC")
library("SnowballC")
clean_corpus <- tm_map(clean_corpus, stemDocument)
pal<-brewer.pal(9,'Dark2')


# creating a word cloud for words from complaints for "support" 
wordcloud(clean_corpus[contact$category=='support'],min.freq=5,random.order = F,colors = pal)

# creating a word cloud for words form complatins for "sales"
wordcloud(clean_corpus[contact$category=='sales'],min.freq = 5,random.order = F,colors = pal)

# creating a function for convert binaric values to 'yes' and 'no'
convert_yesno<-function(x){
  if(x==0)return ('no')
  return('yes')
}

yesno_matrix<-apply(frequent_dtm,MARGIN =1:2,convert_yesno)

contact.df<-as.data.frame(yesno_matrix)
str(contact.df)

# returning the labels
contact.df$category<-contact$category
str(contact.df)
dim(contact.df)

# creating a vector with 500 random numbers
random<-runif(500)
# we will use only the numbers larger then 0.3
filter<-random>0.3

#spliting data set into train and test 
contact.df.train<-contact.df[filter,]
contact.df.test<-contact.df[!filter,]
dim(contact.df.train)
dim(contact.df.test)

install.packages('e1071')
library(e1071)


# creating a naive bayes model
model<-naiveBayes(contact.df.train[,-71],contact.df.train$category)

# checking the model on the test set with support complaints
prediction<-predict(model,contact.df.test[,-71],type = 'raw')
prediction_contact<-prediction[,'support']
actual<-contact.df.test$category
predicted<-prediction_contact>0.4

#confusion matrix
conf.matrix<-table(predicted,actual)

precision<-conf.matrix[2,2]/(conf.matrix[2,2]+conf.matrix[2,1])
precision
recall<-conf.matrix[2,2]/(conf.matrix[2,2]+conf.matrix[1,2])
recall
 
